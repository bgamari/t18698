#!/usr/bin/env bash

set -e

#GHC=$(nix eval --raw nixpkgs.haskell.compiler.ghc881)/bin/ghc
GHC=$(nix eval --raw nixpkgs.haskell.compiler.ghc8101)/bin/ghc
#GHC=$HOME/ghc/ghc-9.0/_build/stage1/bin/ghc

run() {
  local name="$1"
  shift
  local args="-ddump-timings -fforce-recomp -ddump-simpl -dsuppress-coercions -ddump-to-file -dsuppress-uniques +RTS -s -RTS"
  $GHC -O2 Blowup.hs $args -dumpdir $name $@
}

run good
run bad -DCOERCE

